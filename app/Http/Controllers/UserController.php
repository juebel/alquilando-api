<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return UserService::getAll();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => ['string','max:255','required'],
            'apellido' => ['string','max:255','required'],
            'email' => ['email','required',Rule::unique('users')],
            'usuario' => ['string','max:255','required',Rule::unique('users')]
        ]);

        $userData = $request->input();
        try {

            return UserService::saveOne($userData);

        } catch (ModelNotFoundException $e) {
            $response = [
                "message" => "User Not found",
                ];
            return response()->json($response,404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        try {

            return UserService::getOne($user_id);

        } catch (ModelNotFoundException $e) {
            $response = [
                "message" => "User Not found",
                ];
            return response()->json($response,404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id)
    {

        $request->validate([
            'nombre' => ['string','max:255','required'],
            'apellido' => ['string','max:255','required'],
            'email' => ['email','required',Rule::unique('users')->ignore($user_id)],
            'usuario' => ['string','max:255','required',Rule::unique('users')->ignore($user_id)]
        ]);

        $userData = $request->input();
        try {

            return UserService::updateOne($userData, $user_id);

        } catch (ModelNotFoundException $e) {
            $response = [
                "message" => "User Not found",
                ];
            return response()->json($response,404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id)
    {
        try {

            return UserService::deleteOne($user_id);

        } catch (ModelNotFoundException $e) {
            $response = [
                "message" => "User Not found",
                ];
            return response()->json($response,404);
        }
    }
}
