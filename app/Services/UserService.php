<?php

namespace App\Services;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;

class UserService extends Controller
{
    static function getAll() {
    	return User::all();
    }

    static function saveOne($userData) {
    	return User::create($userData);
    }

    static function getOne($user_id) {
    		return User::findOrFail($user_id);
    }

    static function updateOne($userData, $user_id) {
    		$user = User::findOrFail($user_id);
    		$user->fill($userData);
    		$user->save();
    		return $user;
    }

    static function deleteOne($user_id) {
    		$user = User::findOrFail($user_id)->delete();
    }


}
