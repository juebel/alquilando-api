<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://alquilando.com/assets/header/alquilando-logo-green.svg" width="400"></a></p>


## About Alquilando API

API de ejemplo para Alquilando.com:

- Creada en [Laravel Framework 8.38.0](https://laravel.com/docs/8.x).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Deploy


- Clonar el proyecto

```bash
git clone git@gitlab.com:juebel/alquilando-api.git
```

- Ingresar al direcorio del proyecto

```bash
cd alquilando-api
```

- Instalar dependencias de Composer

```bash
composer install
```

- Copiar el archivo .env.example a .env

```bash
cp .env.example .env
```

- Configurar la BBDD en el archivo .env *con una bbdd, usuario y claves existentes*

```bash
vim .env
```

```
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=tpalquilando
DB_USERNAME=developer
DB_PASSWORD=developer
```

- Ejecutar las migraciones

```bash
php artisan migrate:fresh
```

- Iniciar el servidor en el {host} y {puerto} preferidos
php artisan serve --host {host} --port {puerto}

Ejemplo:
```bash
php artisan serve --host 0.0.0.0 --port 10007
```

- Test en Postman

Para usar el proyecto Postman, cambar la variable {{baseUrl}} por el valor host:puerto en el que se publica el proyecto, ejemplo: "192.168.100.130:10007" 

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/2868426-ca7b743d-3146-4077-9e63-87b88f3697b4?action=collection%2Ffork&collection-url=entityId%3D2868426-ca7b743d-3146-4077-9e63-87b88f3697b4%26entityType%3Dcollection#?env%5BALQUILANDO-DEV%5D=W3sia2V5IjoiYmFzZVVybCIsInZhbHVlIjoiMTkyLjE2OC43Mi4xMzA6MTAwMDgiLCJlbmFibGVkIjp0cnVlfV0=)